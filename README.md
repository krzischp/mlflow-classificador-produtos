[Link do projeto no GitLab](https://gitlab.com/krzischp/mlflow-classificador-produtos)
[Versao local do MLFlow para executar em paralelo](https://github.com/krzischp/mlflow-server)

# Rodar no local

```bash
conda create --name MLflow python=3.10.4
conda activate MLflow
pip install mlflow
export MLFLOW_TRACKING_URI=http://localhost:5000
export MLFLOW_EXPERIMENT_NAME=classificador-produtos-sem-devops
mlflow run .
```
O comando `mlflow run .` irá procurar, na pasta local, pelo arquivo MLproject, e irá disparar a execução.



# Rodar experimentos

```bash
export MLFLOW_TRACKING_URI=http://localhost:5000
export MLFLOW_EXPERIMENT_NAME=mlflow-classificador-produtos

mlflow run -v main https://gitlab.com/krzischp/mlflow-classificador-produtos.git -P test_size=0.1 -P include_names=sim
mlflow run -v main https://gitlab.com/krzischp/mlflow-classificador-produtos.git -P test_size=0.2 -P include_names=sim
mlflow run -v main https://gitlab.com/krzischp/mlflow-classificador-produtos.git -P test_size=0.3 -P include_names=sim
mlflow run -v main https://gitlab.com/krzischp/mlflow-classificador-produtos.git -P test_size=0.4 -P include_names=sim
mlflow run -v main https://gitlab.com/krzischp/mlflow-classificador-produtos.git -P test_size=0.5 -P include_names=sim
mlflow run -v main https://gitlab.com/krzischp/mlflow-classificador-produtos.git -P test_size=0.1 -P include_names=não
mlflow run -v main https://gitlab.com/krzischp/mlflow-classificador-produtos.git -P test_size=0.2 -P include_names=não
mlflow run -v main https://gitlab.com/krzischp/mlflow-classificador-produtos.git -P test_size=0.3 -P include_names=não
mlflow run -v main https://gitlab.com/krzischp/mlflow-classificador-produtos.git -P test_size=0.4 -P include_names=não
mlflow run -v main https://gitlab.com/krzischp/mlflow-classificador-produtos.git -P test_size=0.5 -P include_names=não
```

Podemos ver o resultado das execuções na interface visual. Abra o endereço no navegador: http://localhost:5000.

Veja como os commits no GitLab aparecem nessa interface também, para possibilitar a identificação da versão do código que originou cada experimento.
